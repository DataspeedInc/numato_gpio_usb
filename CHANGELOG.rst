^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package numato_gpio_usb
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.0 (2024-05-14)
------------------
* Migrate to ROS2
* Contributors: Gabe Oetjens
