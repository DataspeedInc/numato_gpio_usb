#!/usr/bin/env python3
######################################################################
# Software License Agreement (BSD License)
#
#  Copyright (c) 2018, Dataspeed Inc.
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#   * Neither the name of Dataspeed Inc. nor the names of its
#     contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
######################################################################

# Name: gpio_node.py
# Purpose: Provide simple access to analog and digital values and configuration of
#   Numato's usb gpio devices
# TODO: make this documentation follow some sort of python standard
# Reference sites:
# https://numato.com/docs/8-channel-usb-gpio-module-with-analog-inputs/
# https://numato.com/docs/16-channel-usb-gpio-module-with-analog-inputs/
# https://numato.com/docs/32-channel-usb-gpio-module-with-analog-inputs/
# https://numato.com/docs/64-channel-usb-gpio-module-analog-inputs/

# import rospy
import rclpy
from rclpy.parameter import Parameter
from rclpy.qos import QoSProfile
from rclpy.signals import SignalHandlerOptions
import rclpy.logging
import serial
import threading
import sys

from std_msgs.msg import Float32
from std_msgs.msg import UInt64
from std_msgs.msg import String

lock = threading.Lock()

logger = rclpy.logging.get_logger("numato_gpio_usb")

# wrap serial calls to handle serial exceptions attempting to reopen serial upon failure
# Tested by unplugging and plugging in gpio_usb device while node is running
def ser_exec(f, *args):
   while lock.locked():
       pass
   with lock:
        try:
            if ser_exec.ser == None:
                ser_exec.ser = serial.Serial(node.dev, 19200, timeout=0.01)
                logger.info("opened gpio serial port")
                node.io_dir_set = False # force I/O direction to be reconfigured, since board was likely a reset
        except serial.SerialException as msg:
            logger.error("error opening serial port: %s " % (msg), throttle_duration_sec=5)
            return None
        try:
            res = f(ser_exec.ser, *args)

            if res == None:
                logger.warn(f"serial function {f.__name__}{args} didn't have a value to return")

            return res
        except Exception as error:
            # logger.error("error using serial port, was the device removed?", throttle_duration_sec=1)
            logger.error(f"serial function {f.__name__}{args}:\n\terror using serial port: {error}", throttle_duration_sec=1)
            ser_exec.ser = None # remember that we have to reopen this next time
            return None
ser_exec.ser = None

def _read_analog(ser, ch):
    ser.write(("adc read %d\r" % ch).encode())
    resp = ser.read(25)[10:-3]

    return float(resp)/1024.0 if len(resp) > 0 else None

def read_analog(ch):
    return ser_exec(_read_analog, ch)

def _read_digital(ser):
    ser.write(("gpio readall\r").encode())
    # fullresp = ser.read(64)
    # resp = ser.read(64)[10:-3]
    resp = ser.read(64)[12:-3]
    # logger.info(f"_read_digital() fullresp: {fullresp} resp: {resp}")

    return int(resp, 16) if len(resp) > 0 else None

def read_digital():
    return ser_exec(_read_digital)

def _write_digital(ser, state, pin):
    HIGH = 1
    LOW = 0
    if state == HIGH:
        ser.write(("gpio set %s\r" % str(pin)).encode())
    else:
        ser.write(("gpio clear %s\r" % str(pin)).encode())

    return True

def write_digital(state, pin):
    return ser_exec(_write_digital, state, pin)

def _set_io_dir(ser):
    ser.write(("gpio iodir %s\r" % (node.io_dir)).encode())
    ser.read(64)

    return True

def set_io_dir():
    ser_exec(_set_io_dir)

def output_channels():
    OUT = '0'
    return [ i for (i, dir) in enumerate(node.io_bits) if dir == OUT ]

def input_channels():
    IN = '1'
    return [ i for (i, dir) in enumerate(node.io_bits) if dir == IN ]

#Send "gpio read" command.
#GPIO number 10 and beyond are referenced in the command by using alphabets starting A.
#For example GPIO10 will be A, GPIO11 will be B and so on.
#Please note that this is not intended to be hexadecimal notation
#so the the alphabets can go beyond F.
#format - pin hi/low - "01", "70", "71" , "R1" , "T1" etc.
def outputCallback(data):

    if len(data.data) == 2:
        gpioIndex = int(data.data[0])
        state = int(data.data[1])
    elif len(data.data) == 3:
        pin1 = int(data.data[0])  # 2
        pin2 = int(data.data[1])  # 9
        state = int(data.data[2])
        gpioNum = str(pin1) + str(pin2) # 29

        if (int(gpioNum) < 10):
            gpioIndex = str(gpioNum)
        else:
            gpioIndex = chr(55 + int(gpioNum))
    write_digital(state, gpioIndex)

# All of the different boards with (8, 16, 32, 64) I/Os have different analog channel to port mappings
# use the adc_map to handle the different configurations for (8, 16, 32, 64) channel boards
adc_map = {}
adc_map[8] = [ 0, 1, 2, 3, None, None, 4, 5]
adc_map[16] = list(range(7)) + 9 * [None]
adc_map[32] = [None] + list(range(1,8)) + 24 * [None]
adc_map[64] = list(range(32)) + 32 * [None]

def analog_channels():
    bd_adc_map = adc_map[node.num_io]
    return [ bd_adc_map[ch] for ch in input_channels() if bd_adc_map[ch] != None ]

def node():
    rclpy.init(args=sys.argv, signal_handler_options=SignalHandlerOptions.NO)

    node.node_instance = rclpy.create_node("gpio")

    # Declare parameters
    node.node_instance.declare_parameter("dev",     rclpy.Parameter.Type.STRING)
    node.node_instance.declare_parameter("num_io",  rclpy.Parameter.Type.INTEGER)
    node.node_instance.declare_parameter("io_dir",  rclpy.Parameter.Type.STRING)
    node.node_instance.declare_parameter("serial",  rclpy.Parameter.Type.STRING)
    node.node_instance.declare_parameter("adc_ref", rclpy.Parameter.Type.DOUBLE)
    node.node_instance.declare_parameter("rate",    rclpy.Parameter.Type.DOUBLE)

    node.dev = node.node_instance.get_parameter_or('dev', Parameter('', value='/dev/ttyACM0')).value
    node.num_io = int(node.node_instance.get_parameter_or('num_io', Parameter('', value='8')).value)
    if node.num_io == 8:
        node.io_dir = node.node_instance.get_parameter_or('io_dir', Parameter('', value='FF')).value.lower()
    elif node.num_io == 16:
        node.io_dir = node.node_instance.get_parameter_or('io_dir', Parameter('', value='FFFF')).value.lower()
    elif node.num_io == 32:
        node.io_dir = node.node_instance.get_parameter_or('io_dir', Parameter('', value='FFFFFFFF')).value.lower()
    elif node.num_io == 64:
        node.io_dir = node.node_instance.get_parameter_or('io_dir', Parameter('', value='FFFFFFFFFFFFFFFF')).value.lower()
    else:
        logger.fatal("%d is not supported" % node.num_io)
        return None

    if node.num_io != len(node.io_dir) * 4:
        logger.fatal("length of io_dir, %d bits (specified in hex), does not match num_io, %d" % (len(node.io_dir)*4, node.num_io))
        return None
    #TODO: use serial
    node.serial_num = node.node_instance.get_parameter_or('serial', Parameter('', value='')).value

    node.io_dir_set = False # force I/O dir to be configured

    # node.adc_ref = float(node.node_instance.get_parameter_or('adc_ref', '3.3').value)
    node.adc_ref = float(node.node_instance.get_parameter_or('adc_ref', Parameter('', value='3.3')).value)

    # From testing, the max measurement rate in Hz can be determined by:
    #   1000/(10.5 + 10.4*NUM_SUBSCRIBED_TOPICS)
    # where NUM_SUBSCRIBED_TOPICS is the total number of input topics that have
    # at least one subscriber. So if all 6 analogs and the digital input are
    # subscribed to, the max rate is about 12 Hz.
    # This does not take into account any output messages.
    rate_hz = float(node.node_instance.get_parameter_or('rate', Parameter('', value='10.0')).value)

    # make sure num_io is valid
    # TODO: num_io could potentially be auto discovered
    try:
        adc_map[node.num_io]
    except:
        valid_num_ios = [num_io for (num_io, _) in adc_map.items()]
        logger.fatal("%d is not a valid number of I/Os, valid number of I/Os are %s" % (node.num_io, valid_num_ios) )
        return None # TODO: better way to shutdown?

    # order the io bits in a left to right config for simple [0 to num_ios-1] indexing
    # this allows us to figure out input and ouput configurations ('0' = output, '1' = input)
    node.io_bits = bin(int(node.io_dir, 16))[::-1][0:node.num_io]

    logger.fatal("inputs: %s" % input_channels())
    logger.fatal("adc_inputs: %s" % analog_channels())
    logger.fatal("outputs: %s" % output_channels())
    logger.fatal("adc_ref: %sV" % node.adc_ref)
    logger.fatal("rate: %sHz" % rate_hz)

    node.node_instance.create_subscription(String, "output_write", outputCallback, qos_profile=QoSProfile(depth=10))

    analog_pubs = [ (ch, node.node_instance.create_publisher(Float32, 'analog%d' % ch, QoSProfile(depth=10))) for ch in analog_channels() ]
    digital_pub = node.node_instance.create_publisher(UInt64, "digital", QoSProfile(depth=10))

    while rclpy.ok():
        if not node.io_dir_set:
            set_io_dir()
        if digital_pub.get_subscription_count():
            ddata = read_digital()
            if ddata is not None:
                digital_pub.publish(UInt64(data=ddata))
        for (ch, analog_pub) in analog_pubs:
            if analog_pub.get_subscription_count():
                adata = read_analog(ch)
                if adata is not None:
                    analog_pub.publish(Float32(data=(adata * node.adc_ref)))
        rclpy.spin_once(node.node_instance, timeout_sec=1/rate_hz)

    ser_exec.ser = None
    return None

if __name__ == '__main__':
    try:
        node()
    except KeyboardInterrupt:
        pass

    rclpy.try_shutdown()