# numato_gpio_usb

ROS interface to Numato GPIO USB boards for automation

* 8 Channel USB GPIO Module With Analog Inputs
    * https://numato.com/docs/8-channel-usb-gpio-module-with-analog-inputs/
    * https://www.amazon.com/dp/B00MXKA45Q
* 16 Channel USB GPIO Module With Analog Inputs
    * https://numato.com/docs/16-channel-usb-gpio-module-with-analog-inputs/
    * https://www.amazon.com/dp/B00NXKJHTY
* 32 Channel USB GPIO Module With Analog Inputs
    * https://numato.com/docs/32-channel-usb-gpio-module-with-analog-inputs/
    * https://www.amazon.com/dp/B00MXJU6PK
* 64 Channel USB GPIO Module With Analog Inputs
    * https://numato.com/docs/64-channel-usb-gpio-module-analog-inputs/
    * https://www.amazon.com/dp/B0762JMGBH

# Parameters
* `serial` Explicitly specify device serial number. Default `<empty>`. TODO: serial is not currently used.
* `dev` Explicitly specify usb device. Default `/dev/ttyACM0`
* `io_dir` IO Direction in hex where Msb is the highest channel and Lsb is channel 0, a 1 indicates and a 0 indpicates output. Default `FF`
* `num_io` Number of IOs on device (8, 16, 32, 64) required to match # of bits in io_direction. Default `8`.
* `adc_ref` Analog reference voltage. Default `3.3`

# Published topics
* `digital`, [std_msgs/UInt64](http://docs.ros.org/api/std_msgs/html/msg/UInt64.html), where the bit# corresponds to the digital channel. Bits are numbered from LSb=0.
* `analog*`, [std_msgs/Float32](http://docs.ros.org/api/std_msgs/html/msg/Float32.html), value of each analog channel configured as an input. # corresponds to analog channel # and not the pin number which is likely different.

# Subscribed topics
* `output_write`, [std_msgs/String](http://docs.ros.org/api/std_msgs/html/msg/String.html), 2-3 digits, first 1-2 digits are channel number (0-1R), last digit is 0/1 for low/high

